﻿using System;
using System.Runtime.InteropServices;

namespace MCCTimeKeepingService
{
    class CKT
    {
        public struct CLOCKINGRECORD
        {
            [MarshalAs(UnmanagedType.I4)]
            public int ID;
            [MarshalAs(UnmanagedType.I4)]
            public int PersonID;
            [MarshalAs(UnmanagedType.I4)]
            public int Stat;
            [MarshalAs(UnmanagedType.I4)]
            public int BackupCode;
            [MarshalAs(UnmanagedType.I4)]
            public int WorkTyte;
            //<VBFixedString(20), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=20)> _
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public byte[] Time;
        }

        [DllImport("tc400.dll", EntryPoint = "CKT_RegisterNet", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int RegisterNet(int Sno, string Addr);

        [DllImport("tc400.dll", EntryPoint = "CKT_UnregisterSnoNet", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern void UnregisterSnoNet(int Sno);

        [DllImport("tc400.dll", EntryPoint = "CKT_ReadRealtimeClocking", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int ReadRealtimeClocking(ref int ppPersons);

        [DllImport("tc400.dll", EntryPoint = "CKT_GetClockingRecordEx", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int GetClockingRecordEx(int Sno, ref int ppLongRun);

        [DllImport("tc400.dll", EntryPoint = "CKT_GetClockingNewRecordEx", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int GetClockingNewRecordEx(int Sno, ref int ppLongRun);

        [DllImport("tc400.dll", EntryPoint = "CKT_SetRealtimeMode", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int SetRealtimeMode(int Sno, int RealMode);

        [DllImport("tc400.dll", EntryPoint = "CKT_GetCounts", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int GetCounts(int Sno, ref int pPersonCount, ref int pFPCount, ref int pClockingsCount);

        [DllImport("tc400.dll", EntryPoint = "CKT_GetClockingRecordProgress", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int GetClockingRecordProgress(int pLongRun, ref int pRecCount, ref int pRetCount, ref int ppPersons);

        [DllImport("tc400.dll", EntryPoint = "CKT_FreeMemory", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern int FreeMemory(int memory);

        [DllImport("kernel32", EntryPoint = "RtlMoveMemory", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern void PCopyMemory(ref CLOCKINGRECORD Destination, int Source, int Length);

        public static Int16 GetClockingRecordSize()
        {
            return Convert.ToInt16(Marshal.SizeOf(new CLOCKINGRECORD()));
        }
    }
}
