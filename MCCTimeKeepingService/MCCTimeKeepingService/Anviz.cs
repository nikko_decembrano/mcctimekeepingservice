﻿using MCCTimeKeepingService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace MCCTimeKeepingService
{
    class Anviz
    {
        public List<ClockEntry> BatchList { get; set; }

        public string IPAddress { get; private set; }
        public int MachineID { get; private set; }

        private int pLongRun { get; set; }
        private bool ended { get; set; }

        public Anviz(string ip_address, int machine_id)
        {
            IPAddress = ip_address;
            MachineID = machine_id;
        }

        public void Connect()
        {
            CKT.RegisterNet(MachineID, IPAddress);
            ended = false;
        }

        public bool SetRealtimeMode()
        {
            return CKT.SetRealtimeMode(MachineID, 1) != 0;
        }

        public void Disconnect()
        {
            CKT.UnregisterSnoNet(MachineID);
        }

        [HandleProcessCorruptedStateExceptions]
        public bool Start()
        {
            int ppLongRun = 0;
            Connect();
            var data = 0;

            try
            {
                data = CKT.GetClockingRecordEx(MachineID, ref ppLongRun);
            }
            catch (AccessViolationException ex)
            {
                throw new Exception(ex.Message);
            }
            
            pLongRun = ppLongRun;
            return data != 0;
        }

        public bool GetBatch()
        {
            return GetBatch(3);
        }

        public bool GetBatch(int tryCount)
        {
            bool data = false;

            for (int ctr = 0; ctr < tryCount; ctr++)
            {
                if (GetBatch(ctr == tryCount - 1))
                {
                    data = true;
                    break;
                }
            }

            return data;
        }

        [HandleProcessCorruptedStateExceptions]
        public bool GetBatch(bool markEndWhenStopped)
        {
            if (!ended)
            {
                BatchList = new List<ClockEntry>();
                int pClockings = 0;
                int RetCount = 0;
                int RecordCount = 0;
                CKT.CLOCKINGRECORD clocking = new CKT.CLOCKINGRECORD();
                int ret = -1;

                try
                {
                    ret = CKT.GetClockingRecordProgress(pLongRun, ref RecordCount, ref RetCount, ref pClockings);
                }
                catch (AccessViolationException ex)
                {
                    throw new Exception(ex.Message);
                }

                int ptemp = pClockings;

                for (int ctr = 0; ctr < RetCount; ctr++)
                {
                    CKT.PCopyMemory(ref clocking, pClockings, CKT.GetClockingRecordSize());
                    pClockings = pClockings + CKT.GetClockingRecordSize();
                    var clockEntry = new ClockEntry();
                    clockEntry.biometric_id = clocking.PersonID.ToString();
                    clockEntry.clock_date = DateTime.Parse(Encoding.Default.GetString(clocking.Time)).Date;
                    clockEntry.clock_time = DateTime.Parse(Encoding.Default.GetString(clocking.Time)).TimeOfDay;
                    clockEntry.hint = clocking.Stat.ToString();
                    /*
                    clockEntry.WorkTyte = clocking.WorkTyte;
                    clockEntry.ID = clocking.ID;
                    clockEntry.BackupCode = clocking.BackupCode;
                    */
                    BatchList.Add(clockEntry);
                }

                if (ptemp != 0)
                {
                    CKT.FreeMemory(ptemp);
                }

                if (ret == 0 || ret == 1)
                {
                    if (markEndWhenStopped)
                    {
                        ended = true;
                    }
                }
            }

            return !ended;
        }

        public int GetCount()
        {
            int personCount = 0;
            int printCount = 0;
            int clockCount = 0;

            CKT.GetCounts(MachineID, ref personCount, ref printCount, ref clockCount);

            return clockCount;
        }
    }
}
