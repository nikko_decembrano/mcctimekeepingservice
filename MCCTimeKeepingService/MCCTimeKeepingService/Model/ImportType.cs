﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Livingstone.DBLib.DBHandlerMy;

namespace MCCTimeKeepingService.Model
{
    class ImportType
    {
        public ulong id_importtype { get; internal set; }
        public string ip_address { get; internal set; }
        public int machine_id { get; internal set; }
        public DateTime modified_date { get; internal set; }
        public string hint_type { get; internal set; }

        internal static ImportType GetById(ulong id_importtype)
        {
            string sql = @"
                SELECT
                    id_importtype,
                    ip_address,
                    machine_id,
                    modified_date,
                    hint_type
                FROM tblimporttype
                WHERE id_importtype=@id_importtype
            ";

            var parameters = new Dictionary<string, object>();
            parameters.Add("id_importtype", id_importtype);

            var data = default(ImportType);

            DataTable dataTable = new DataTable();
            DBHandlerMy.getDataList(dataTable, sql, Servers.exacthk2, parameters);
            if (dataTable.Rows.Count > 0)
            {
                data = Tools.DB.InitFromDataRow<ImportType>(dataTable.Rows[0]);
            }
            dataTable.Dispose();

            return data;
        }
    }
}
