﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Data;
using static Livingstone.DBLib.DBHandlerMy;

namespace MCCTimeKeepingService.Model
{
    class ClockImport
    {
        public ulong id_import { get; internal set; }

        internal static ClockImport GetByBiometricCode(string biometric_code)
        {
            var data = default(ClockImport);

            DataTable dataTable = new DataTable();

            string sql = "CALL GetClockImport ('MCCTimekeepingService', @machine_name, @biometric_code, 'SERVICE');";

            var parameters = new Dictionary<string, object>();
            parameters.Add("machine_name", Environment.MachineName);
            parameters.Add("biometric_code", biometric_code);

            DBHandlerMy.getDataList(dataTable, sql, Servers.exacthk2, parameters);

            if (dataTable.Rows.Count > 0)
            {
                data = Tools.DB.InitFromDataRow<ClockImport>(dataTable.Rows[0]);
            }

            dataTable.Dispose();

            return data;
        }
    }
}
