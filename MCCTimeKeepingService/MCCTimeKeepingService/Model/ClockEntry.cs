﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using static Livingstone.DBLib.DBHandlerMy;

namespace MCCTimeKeepingService.Model
{
    class ClockEntry
    {
        public static List<ClockEntry> ClockEntries;

        public ulong id_import { get; set; }
        public string biometric_id { get; set; }
        public DateTime clock_date { get; set; }
        public TimeSpan clock_time { get; set; }
        public string hint { get; set; }
        public string hint_type { get; set; }

        public static bool Add(ClockEntry clockEntry)
        {
            bool data = false;

            if (ClockEntries == null)
            {
                ClockEntries = new List<ClockEntry>();
            }

            lock (ClockEntries)
            {
                if (!ClockEntries.Any(a =>
                        a.id_import == clockEntry.id_import
                        && a.biometric_id == clockEntry.biometric_id
                        && a.clock_date == clockEntry.clock_date
                        && a.clock_time == clockEntry.clock_time
                        && a.hint == clockEntry.hint
                        && a.hint_type == clockEntry.hint_type
                    )
                )
                {
                    ClockEntries.Add(clockEntry);
                    data = true;
                }
            }

            return data;
        }

        internal void Save()
        {
            if (!ClockEntry.Add(this))
            {
                return;
            }

            var parameters = new Dictionary<string, object>();
            parameters.Add("id_import", id_import);
            parameters.Add("biometric_id", biometric_id);
            parameters.Add("clock_date", clock_date);
            parameters.Add("clock_time", clock_time);
            parameters.Add("hint", hint);
            parameters.Add("hint_type", hint_type);
            parameters.Add("clock_datetime", new DateTime(clock_date.Year, clock_date.Month, clock_date.Day, clock_time.Hours, clock_time.Minutes, clock_time.Seconds));

            string sql_exists = @"
                SELECT id_clock
                FROM tblclockentry
                WHERE biometric_id=@biometric_id
                AND clock_date=@clock_date
                AND clock_time=@clock_time
                AND hint=@hint
                AND hint_type=@hint_type
            ";

            if(DBHandlerMy.getString(sql_exists, Servers.exacthk2, parameters) == "")
            {
                string sql = @"
                    INSERT INTO tblclockentry
                    (
                        id_import,
                        biometric_id,
                        clock_date,
                        clock_time,
                        hint,
                        hint_type
                    )
                    VALUES
                    (
                        @id_import,
                        @biometric_id,
                        @clock_date,
                        @clock_time,
                        @hint,
                        @hint_type
                    );

                    UPDATE tblbiometric AS tb1
                    JOIN tblclockimport AS tb2
                    ON tb2.biometric_code=tb1.biometric_code
                    SET
                        tb1.lastupload_date=NOW(),
                        tb1.latestrecord=@clock_datetime
                    WHERE tb2.id_import=@id_import
                    AND (tb1.latestrecord IS NULL OR tb1.latestrecord<@clock_datetime)
                ";

                DBHandlerMy.ExecuteNonQuery(sql, Servers.exacthk2, parameters);
            }
        }
    }
}
