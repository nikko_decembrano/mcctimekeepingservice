﻿using Livingstone.DBLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Livingstone.DBLib.DBHandlerMy;

namespace MCCTimeKeepingService.Model
{
    class Biometric
    {
        public DateTime encoded_date { get; internal set; }
        public int id_biometric { get; internal set; }
        public string biometric_code { get; internal set; }
        public ulong id_importtype { get; internal set; }

        internal static List<Biometric> GetByType(string file_type)
        {
            string sql = @"
                SELECT
                    tb1.id_biometric,
	                tb1.biometric_code,
	                tb1.id_importtype,
	                tb1.encoded_date
                FROM tblbiometric AS tb1
                JOIN tblimporttype AS tb2
                ON tb2.id_importtype=tb1.id_importtype
                WHERE tb2.file_type=@file_type
            ";

            var parameters = new Dictionary<string, object>();
            parameters.Add("file_type", file_type);

            DataTable dataTable = new DataTable();
            var data = new List<Biometric>();

            DBHandlerMy.getDataList(dataTable, sql, Servers.exacthk2, parameters);

            foreach(DataRow dataRow in dataTable.Rows)
            {
                data.Add(Tools.DB.InitFromDataRow<Biometric>(dataRow));
            }

            dataTable.Dispose();

            return data;
        }

        internal void UpdateDownloadCount(int count)
        {
            string sql = @"
                UPDATE tblbiometric
                SET download_count=@count
                WHERE id_biometric=@id
            ";

            var parameters = new Dictionary<string, object>();
            parameters.Add("count", count);
            parameters.Add("id", id_biometric);

            DBHandlerMy.ExecuteNonQuery(sql, Servers.exacthk2, parameters);
        }
    }
}
