﻿using MCCTimeKeepingService.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Timers;

namespace MCCTimeKeepingService
{
    public partial class AnvizDownloader : ServiceBase
    {
        private Dictionary<int, Biometric> BiometricList;
        private Dictionary<int, BackgroundWorker> BackgroundWorkerPool;
        private Timer BiometricList_ReloadTimer;

        public AnvizDownloader()
        {
            InitializeComponent();
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Log((e.ExceptionObject as Exception).Message);
            }
            catch { }
        }

        protected override void OnStart(string[] args)
        {
            Log("AnvizDownloader started");

            Initialize();
        }

        protected override void OnStop()
        {
            foreach (var backgroundWorker in BackgroundWorkerPool)
            {
                backgroundWorker.Value.CancelAsync();
            }
            Log("AnvizDownloader stopped");
        }

        private void Initialize()
        {
            BackgroundWorkerPool = new Dictionary<int, BackgroundWorker>();
            BiometricList = new Dictionary<int, Biometric>();
            BiometricList_ReloadTimer = new Timer(30 * 1000 * 60);
            BiometricList_ReloadTimer.Enabled = true;
            BiometricList_ReloadTimer.Elapsed += BiometricList_ReloadTimer_Elapsed;
            BiometricList_ReloadTimer.Start();
            ReloadBiometricList();
        }

        private void StartProcess(Biometric biometric)
        {
            var backgroundWorker = new BackgroundWorker();
            BackgroundWorkerPool.Add(biometric.id_biometric, backgroundWorker);
            backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.DoWork += BackgroundWorker_DoWork;
            backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
            var args = new Dictionary<string, object>();
            args.Add("biometric", biometric);
            args.Add("importType", ImportType.GetById(biometric.id_importtype));
            backgroundWorker.RunWorkerAsync(args);
        }

        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var backgroundWorker = (BackgroundWorker)sender;
            var id = (int)e.Result;

            if (e.Error != null)
            {
                Log("Error", e.Error.Message);
            }

            if (BiometricList.ContainsKey(id))
            {
                var args = new Dictionary<string, object>();
                args.Add("biometric", BiometricList[id]);
                args.Add("importType", ImportType.GetById(BiometricList[id].id_importtype));
                backgroundWorker.RunWorkerAsync(args);
            }
            else
            {
                BackgroundWorkerPool.Remove(id);
            }
        }

        private void BiometricList_ReloadTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ReloadBiometricList();
        }

        private void ReloadBiometricList()
        {
            var biometricList = Biometric.GetByType("Anviz");

            //remove
            var keysToDelete = biometricList
                .Where(a => BiometricList.Any(b => b.Value.id_biometric == a.id_biometric))
                .ToList();

            foreach (var keyToDelete in keysToDelete)
            {
                biometricList.Remove(keyToDelete);
            }

            //add or update
            foreach (var biometric in biometricList.Where(a => a.biometric_code == ConfigurationManager.AppSettings["biometric_code"]))
            {
                if (!BiometricList.ContainsKey(biometric.id_biometric))
                {
                    BiometricList.Add(biometric.id_biometric, biometric);
                    StartProcess(biometric);
                }
                else
                {
                    var current = BiometricList
                        .First(a => a.Key == biometric.id_biometric)
                        .Value;

                    if (biometric.encoded_date!= current.encoded_date)
                    {
                        BiometricList[biometric.id_biometric] = biometric;
                    }
                }
            }
        }

        private void Log(string message)
        {
            Log(message, "log.txt");
        }

        private void Log(string message, string filename)
        {
            try
            {
                var streamWriter = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\" + filename, true);
                streamWriter.WriteLine(string.Format("{0:yyyyMMdd HHmmss}: {1}", DateTime.Now, message));
                streamWriter.Flush();
                streamWriter.Close();
            }
            catch { }
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var args = (Dictionary<string, object>)e.Argument;
            var biometric = (Biometric)args["biometric"];

            e.Result = biometric.id_biometric;

            var importType = (ImportType)args["importType"];
            var anviz = new Anviz(importType.ip_address, importType.machine_id);

            try
            {
                anviz.Start();
                var clockImport = ClockImport.GetByBiometricCode(biometric.biometric_code);

                biometric.UpdateDownloadCount(anviz.GetCount());

                while (anviz.GetBatch())
                {
                    foreach (var clockEntry in anviz.BatchList)
                    {
                        if (e.Cancel)
                        {
                            break;
                        }

                        clockEntry.id_import = clockImport.id_import;
                        clockEntry.hint_type = importType.hint_type;
                        clockEntry.Save();
                    }

                    if (e.Cancel)
                    {
                        break;
                    }

                    if (anviz.BatchList.Count == 0)
                    {
                        break;
                    }
                }

                anviz.Disconnect();
            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }

        public void Test(string[] args)
        {
            this.OnStart(args);
            Console.ReadKey();
            this.OnStop();
        }
    }
}
