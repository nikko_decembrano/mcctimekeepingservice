﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;

namespace MCCTimeKeepingService.Tools
{
    public class DB
    {
        public static T InitFromDataRow<T>(DataRow xrow)
        {
            T xret = Activator.CreateInstance<T>();

            FillFromDataRow(xret, xrow);

            return xret;
        }


        public static void FillFromDataRow(object xinstance, DataRow xrow)
        {
            if (xinstance == null
                || xrow == null)
            {
                return;
            }

            Type xtype = xinstance.GetType();

            foreach (PropertyInfo xinfo in xtype.GetProperties())
            {
                if (xrow.Table.Columns.Contains(xinfo.Name))
                {
                    try
                    {
                        if (xinfo.PropertyType.Name == "String")
                        {
                            xinfo.SetValue(xinstance, (xrow[xinfo.Name] ?? "").ToString(), null);
                        }
                        else if (xinfo.PropertyType.Name == "Boolean")
                        {
                            int xtmp = 0;
                            if ((xrow[xinfo.Name] ?? "False").ToString() == "True")
                            {
                                xtmp = 1;
                            }
                            else
                            {
                                int.TryParse((xrow[xinfo.Name] ?? "0").ToString(), out xtmp);
                            }
                            xinfo.SetValue(xinstance, (xtmp != 0), null);
                        }
                        else if (xinfo.PropertyType.Name == "DateTime")
                        {
                            if (xrow[xinfo.Name] != DBNull.Value)
                            {
                                //DateTime dt = DateTime.ParseExact(string.Format("{0:yyyyMMddHHmmssfff}", xrow[xinfo.Name]), "yyyyMMddHHmmssfff", null);
                                DateTime dt;
                                if (!DateTime.TryParseExact(string.Format("{0:yyyyMMddHHmmssfff}", xrow[xinfo.Name]), "yyyyMMddHHmmssfff", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dt))
                                {
                                    if (!DateTime.TryParse(xrow[xinfo.Name].ToString(), out dt))
                                        dt = default(DateTime);
                                }
                                xinfo.SetValue(xinstance, dt, null);
                            }
                            else
                            {
                                xinfo.SetValue(xinstance, default(DateTime), null);
                            }
                        }
                        else if (!(xrow[xinfo.Name] is DBNull))
                        {
                            switch (xinfo.PropertyType.Name)
                            {
                                case "UInt64":
                                    xinfo.SetValue(xinstance, Convert.ToUInt64(xrow[xinfo.Name]), null);
                                    break;
                                default:
                                    xinfo.SetValue(xinstance, xrow[xinfo.Name], null);
                                    break;
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
        /*
        public static object Insert(object xinstance, string xtablename, string xdatabase)
        {
            return DB.Insert(xinstance, null, xtablename, xdatabase);
        }

        public static object Insert(object xinstance, string[] xarr_exclude, string xtablename, string xdatabase)
        {
            object returnValue = null;

            if (xinstance == null)
            {
                return returnValue;
            }

            Type xtype = xinstance.GetType();
            List<PropertyInfo> xlist_info = new List<PropertyInfo>();

            foreach (PropertyInfo xins_info in xtype.GetProperties())
            {
                if (xins_info.GetValue(xinstance, null) is DBNull)
                {
                    continue;
                }
                else if (xins_info.GetValue(xinstance, null) == null)
                {
                    continue;
                }
                else if (xarr_exclude != null && xarr_exclude.Contains(xins_info.Name))
                {
                    continue;
                }
                else if (xins_info.GetCustomAttributes(true).Count(xtmp => xtmp is Exclude) > 0)
                {
                    continue;
                }

                xlist_info.Add(xins_info);
            }
            if (xlist_info.Count == 0)
            {
                return returnValue;
            }

            object[,] xarr_param = new object[xlist_info.Count, 2];

            for (int xctr = 0; xctr < xlist_info.Count; xctr++)
            {
                xarr_param[xctr, 0] = "@" + xlist_info[xctr].Name;
                xarr_param[xctr, 1] = xlist_info[xctr].GetValue(xinstance, null);
            }

            returnValue = DataSource.GetValue(string.Format(@"
                    INSERT INTO {0}
                    (
                        {1}
                    )
                    VALUES
                    (
                        {2}
                    );


                    SELECT @@LAST_INSERT_ID;
                ",
                 xtablename,
                 string.Join(",", xlist_info.Select(xtmp => "`" + xtmp.Name + "`").ToArray()),
                 string.Join(",", xlist_info.Select(xtmp => "@" + xtmp.Name).ToArray())
                ),
                 xdatabase,
                 xarr_param
             );

            return returnValue;
        }

        public static void Update(object xinstance, string xtablename, string xdatabase, string xfilter, params object[] xarr_filter)
        {
            DB.Update(xinstance, null, xtablename, xdatabase, xfilter, xarr_filter);
        }

        public static void Update(object xinstance, string[] xarr_exclude, string xtablename, string xdatabase, string xfilter, params object[] xarr_filter)
        {
            if (xinstance == null)
            {
                return;
            }

            Type xtype = xinstance.GetType();
            List<PropertyInfo> xlist_info = new List<PropertyInfo>();

            foreach (PropertyInfo xins_info in xtype.GetProperties())
            {
                if (xarr_exclude != null && xarr_exclude.Contains(xins_info.Name))
                {
                    continue;
                }
                else if (xins_info.GetCustomAttributes(true).Count(xtmp => xtmp is Exclude) > 0)
                {
                    continue;
                }

                xlist_info.Add(xins_info);
            }
            if (xlist_info.Count == 0)
            {
                return;
            }
            if (xarr_filter == null)
            {
                xarr_filter = new object[0];
            }

            object[,] xarr_param = new object[xlist_info.Count + xarr_filter.Length, 2];

            for (int xctr = 0; xctr < xlist_info.Count; xctr++)
            {
                xarr_param[xctr, 0] = xlist_info[xctr].Name;
                xarr_param[xctr, 1] = xlist_info[xctr].GetValue(xinstance, null);
            }
            for (int xctr = 0; xctr < xarr_filter.Length; xctr++)
            {
                xarr_param[xctr + xlist_info.Count, 0] = "@" + xctr.ToString();
                xarr_param[xctr + xlist_info.Count, 1] = xarr_filter[xctr];
            }
            if ((xfilter ?? "").ToString() == "")
            {
                xfilter = "1=1";
            }

            DataSource.ExecuteNonQuery(string.Format(@"
                    UPDATE {0}
                    SET {1}
                    WHERE {2}
                ",
                 xtablename,
                 string.Join(",",
                    xlist_info.Select(xtmp => "`" + xtmp.Name + "`" + "=" + "@" + xtmp.Name).ToArray()
                 ),
                 xfilter
                ),
                 xdatabase,
                 xarr_param
             );
        }

        public static void Delete(string xtablename, string xdatabase, string xfilter, params object[] xarr_filter)
        {
            object[,] xarr_param = new object[xarr_filter.Length, 2];

            for (int xctr = 0; xctr < xarr_filter.Length; xctr++)
            {
                xarr_param[xctr, 0] = "@" + xctr.ToString();
                xarr_param[xctr, 1] = xarr_filter[xctr];
            }
            if ((xfilter ?? "").ToString() == "")
            {
                xfilter = "1=1";
            }

            DataSource.ExecuteNonQuery(string.Format(@"
                    DELETE
                    FROM {0}
                    WHERE {1}
                ",
                 xtablename,
                 xfilter
                ),
                 xdatabase,
                 xarr_param
             );
        }
        */
        public static List<string> GetEditedFields<T>(T original, T edited, string[] to_compare)
        {
            List<string> xresult = new List<string>();
            Type xtype = typeof(T);

            foreach (PropertyInfo xins_info in xtype.GetProperties())
            {
                if (to_compare.ToList().ConvertAll(s => s.ToLowerInvariant()).Contains(xins_info.Name.ToLower()))
                {
                    object original_val = xins_info.GetValue(original, null);
                    object edited_val = xins_info.GetValue(edited, null);

                    if (xins_info.PropertyType.Name.ToLower().Equals("string"))
                    {
                        if (!(original_val.ToString().Trim()
                            .Equals(edited_val == null ? string.Empty : edited_val.ToString().Trim())))
                        {
                            xresult.Add(xins_info.Name);
                        }
                    }
                    else
                    {
                        if (!(original_val.Equals(edited_val)))
                        {
                            xresult.Add(xins_info.Name);
                        }
                    }
                }
            }

            return xresult;
        }

        //tools
        //public static DbCommand CreateCommand(string xdatabase)
        //{
        //    DbCommand xret = DataSource.getConn(xdatabase).CreateCommand();

        //    if (xret.Connection.State == ConnectionState.Open)
        //    {
        //        xret.Connection.Close();
        //    }
        //    try
        //    {
        //        xret.Connection.Open();
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //    return xret;
        //}
        private static T InitFromSDR<T>(DbDataReader xsdr)
        {
            T xret = Activator.CreateInstance<T>();

            for (int xctr = 0; xctr < xsdr.FieldCount; xctr++)
            {
                List<PropertyInfo> xlist_info = typeof(T)
                    .GetProperties()
                    .Where(xtmp => xtmp.Name.ToLower() == xsdr.GetName(xctr).ToLower())
                    .ToList();

                if (xlist_info.Count == 0)
                {
                    continue;
                }

                //set value
                if (xlist_info[0].PropertyType.Name == "String")
                {
                    xlist_info[0].SetValue(xret, (xsdr[xctr] ?? "").ToString(), null);
                }
                /*
                else if (xlist_info[0].PropertyType.Name == "Image")
                {
                    if (xsdr[xctr] != null
                        && !(xsdr[xctr] is DBNull))
                    {
                        try
                        {
                            xlist_info[0].SetValue(xret, (Bitmap)((new ImageConverter()).ConvertFrom(xsdr[xctr])), null);
                        }
                        catch { }
                    }
                }
                */
                else if (xlist_info[0].PropertyType.Name == "Boolean")
                {
                    int xtmp = 0;
                    if ((xsdr[xctr] ?? "False").ToString() == "True")
                    {
                        xtmp = 1;
                    }
                    else
                    {
                        int.TryParse((xsdr[xctr] ?? "0").ToString(), out xtmp);
                    }
                    xlist_info[0].SetValue(xret, (xtmp != 0), null);
                }
                else if (!(xsdr[xctr] is DBNull))
                {
                    xlist_info[0].SetValue(xret, xsdr[xctr], null);
                }
            }

            return xret;
        }

        //query
        public static List<T> Fetch<T>(DbCommand xcommand)
        {
            if (xcommand.Connection.State != ConnectionState.Open)
            {
                xcommand.Connection.Open();
            }

            List<T> xret = new List<T>();

            DbDataReader xsdr = xcommand.ExecuteReader();
            while (xsdr.Read())
            {
                xret.Add(InitFromSDR<T>(xsdr));
            }
            xsdr.Close();

            return xret;
        }

        public static T Single<T>(DbCommand xcommand)
        {

            if (xcommand.Connection.State != ConnectionState.Open)
            {
                xcommand.Connection.Open();
            }

            T xret = default(T);

            DbDataReader xsdr = xcommand.ExecuteReader();
            if (xsdr.Read())
            {
                xret = InitFromSDR<T>(xsdr);
            }
            xsdr.Close();

            return xret;
        }

        /// <summary>
        /// Reorganize single tupled table to horizontal.
        /// </summary>
        /// <param name="dataTable">field_name (DataColumn.Caption), field_value for columns</param>
        /// <returns></returns>
        public static DataTable Pivot(DataTable dataTable)
        {
            return Pivot(dataTable, "Field Name", "Field Value");
        }

        public static DataTable Pivot(DataTable dataTable, string keycaption, string valuecaption)
        {
            DataTable xret = new DataTable();

            xret.Columns.Add("field_name").Caption = keycaption;
            xret.Columns.Add("field_value").Caption = valuecaption;

            if (dataTable.Rows.Count > 0)
            {
                foreach (DataColumn dataColumn in dataTable.Columns)
                {
                    var dataRowNew = xret.NewRow();

                    if (dataColumn.Caption == "@hide")
                    {
                        continue;
                    }

                    dataRowNew["field_name"] = dataColumn.Caption;
                    dataRowNew["field_value"] = dataTable.Rows[0][dataColumn.ColumnName];

                    xret.Rows.Add(dataRowNew);
                }
            }

            return xret;
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class Exclude : Attribute
    {
        public Exclude()
        {
        }
    }
}
